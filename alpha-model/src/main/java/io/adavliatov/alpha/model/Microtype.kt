package io.adavliatov.alpha.model

abstract class Microtype<T : Any>(val value: T) {
    init {
        checkNotNull(value) { "value can not be empty" }
    }
}