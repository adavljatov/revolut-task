package io.adavliatov.alpha.model.error

import io.adavliatov.alpha.model.error.HttpErrorCode.BAD_REQUEST

open class IllegalArgumentError(errorCode: Int, errorMessage: String) : AppError(BAD_REQUEST, errorCode, errorMessage) {

    constructor(errorCode: Int, errorMessage: String, params: Any?) : this(errorCode, String.format(errorMessage, params))

}