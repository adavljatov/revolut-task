package io.adavliatov.alpha.model.error

import io.adavliatov.alpha.model.error.HttpErrorCode.NOT_IMPLEMENTED

open class ImplementationNotFoundError(errorCode: Int, errorMessage: String) : AppError(NOT_IMPLEMENTED, errorCode, errorMessage) {

    constructor(errorCode: Int, errorMessage: String, params: Any?) : this(errorCode, String.format(errorMessage, params))

}