package io.adavliatov.alpha.model

import java.util.*
import java.util.UUID.randomUUID

class Id<M>(modelClass: Class<M>, id: UUID = randomUUID()) : CommonId<M, UUID>(modelClass, id) {
    fun <M, T> from(id: CommonId<M, T>): Id<M> {
        if (id.id is UUID) return Id(id.modelClass, id.id)
        throw IllegalArgumentException("Invalid CommonId class, should be UUID instead")
    }

    companion object {
        fun <M> randomId(modelClass: Class<M>): Id<M> = Id(modelClass)
    }
}