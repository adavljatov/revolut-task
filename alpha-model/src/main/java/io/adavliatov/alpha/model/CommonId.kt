package io.adavliatov.alpha.model

open class CommonId<M, T>(val modelClass: Class<M>, val id: T) {

}