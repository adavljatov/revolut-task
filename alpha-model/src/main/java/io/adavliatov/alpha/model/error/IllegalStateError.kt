package io.adavliatov.alpha.model.error

import io.adavliatov.alpha.model.error.HttpErrorCode.UNAUTHORIZED

open class IllegalStateError(errorCode: Int, errorMessage: String) : AppError(UNAUTHORIZED, errorCode, errorMessage) {

    constructor(errorCode: Int, errorMessage: String, params: Any?) : this(errorCode, String.format(errorMessage, params))

}