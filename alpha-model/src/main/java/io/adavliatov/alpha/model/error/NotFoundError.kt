package io.adavliatov.alpha.model.error

import io.adavliatov.alpha.model.error.HttpErrorCode.NOT_FOUND

open class NotFoundError(errorCode: Int, errorMessage: String) : AppError(NOT_FOUND, errorCode, errorMessage) {

    constructor(errorCode: Int, errorMessage: String, params: Any?) : this(errorCode, String.format(errorMessage, params))

}