package io.adavliatov.alpha.model

abstract class Model<M, S>(val id: Id<M>, val state: S)
        where M : Model<M, S>, S : State<M> {
}

abstract class ModelWithoutState<M : Model<M, NoState<M>>>(id: Id<M>) : Model<M, NoState<M>>(id, NoState<M>())