package io.adavliatov.alpha.model.error

import io.adavliatov.alpha.model.error.HttpErrorCode.INTERNAL_SERVER_ERROR

open class InternalError(errorCode: Int, errorMessage: String) : AppError(INTERNAL_SERVER_ERROR, errorCode, errorMessage) {

    constructor(errorCode: Int, errorMessage: String, params: Any?) : this(errorCode, String.format(errorMessage, params))

}