package io.adavliatov.alpha.model

sealed class CommonState<M : Model<M, CommonState<M>>> : State<M> {
    class DELETED<M : Model<M, CommonState<M>>> : CommonState<M>()
    class ACTIVE<M : Model<M, CommonState<M>>> : CommonState<M>()
}
