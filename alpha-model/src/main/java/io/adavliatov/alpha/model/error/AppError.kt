package io.adavliatov.alpha.model.error

open class AppError(val httpErrorCode: HttpErrorCode, val errorCode: Int, errorMessage: String) : RuntimeException(errorMessage) {

    constructor(httpErrorCode: HttpErrorCode, errorCode: Int, errorMessage: String, params: Any?) : this(httpErrorCode, errorCode, String.format(errorMessage, params))

}