package io.adavliatov.alpha.model

class NoState<M : Model<M, NoState<M>>> : State<M> {
}