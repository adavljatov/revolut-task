# README #

## Description ##

Revolut test task.

Design and implement a RESTful API (including data com.revolut.transaction.model and the backing implementation) for money transfers between internal users/accounts.

## Requirements: ##

1. keep it simple and to the point (e.g. no need to implement any authentication, assume the APi is invoked by another internal system/com.revolut.transaction.service);
2. use whatever frameworks/libraries you like (except Spring) but don't forget about the requirement #1;
3. the datastore should run in­memory for the sake of this test;
4. the final result should be executable as a standalone program (should not require a pre­installed container/server);
5. demonstrate with tests that the API works as expected.

## Technologies stack ##

### Core ###

* [Gradle](https://gradle.org/) - build tool;
* [Google Guice](https://github.com/google/guice) - dependency management;
* [Spark Framework](sparkjava.com) - REST endpoint.

### Test ###

* [JUnit5](junit.org/junit5) - test framework;
* [Mockito](site.mockito.org) - mock framework.

### Helper libs ###

* [Gson](https://github.com/google/gson) - JSON to/from serialization;
* [Lombok](https://projectlombok.org) - reduce code boilerplate;
* [Apache Commons](https://commons.apache.org) - a set of helper util libs.

## Configure & Run application ##

### Windows ###

`gradle.bat run [-PlaunchParams="[<port>,<partition size>]"]`

#### Examples ####

1. gradle.bat run
2. gradle.bat run -PlaunchParams="['1234','3']

### Unix (-like) ###

`./gradle run [-PlaunchParams="[<port>,<partition size>]"]`

#### Examples ####

1. ./gradle run
2. ./gradle run -PlaunchParams="['1234','3']"

## Application overview ##

### Common ###

Spark listens for incoming requests and routes them to the specific service.

`Account service` is responsible for management account-related requests: create/find. java.util.Concurrent map is used under the hood as the storage, partitioning based on name is implemented.

`Transaction service` handles deposit/withdraw/transfer requests in a thread-safe manner and returns the `Transaction` response representing the transaction result.

### REST endpoints ###

#### Account API ####

* `PUT : /api/account/:name` - gets or creates new account with specified :name;
* `GET : /api/account/:name` - searches account by :name;

#### Transactions API ####

* `POST: /api/tx/deposit/:name/:amount` - deposits the :amount to the :account;
* `POST: /api/tx/withdraw/:name/:amount` - withdraw the :amount from the :account;
* `POST: /api/tx//transfer/:from/:to/:amount` - transfers the amount from :from account to the :to account.

### NB ###

Possible extension points are marked with corresponding comments.
All assumptions (e.g. Integer as the amount type) are presented in the comments, too.

## Authors ##

 Aleksandr Davliatov

* email: alexander.davliatov@gmail.com
* skype: alex_davljatov