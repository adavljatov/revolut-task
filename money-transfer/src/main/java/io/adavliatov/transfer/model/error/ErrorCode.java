package io.adavliatov.transfer.model.error;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum ErrorCode {
    DUPLICATE_ACCOUNT(10, "The same account specified: %s. You must use different account name."),
    ACCOUNT_NOT_FOUND(20, "Account(s) not found: %s."),
    INVALID_ACCOUNT_NAME(30, "Invalid account name: %s . Should starts with alpha & contains alphanumeric only"),
    INVALID_AMOUNT(40, "Invalid balance: %s - must be a positive decimal."),;

    final int code;
    final String message;
}
