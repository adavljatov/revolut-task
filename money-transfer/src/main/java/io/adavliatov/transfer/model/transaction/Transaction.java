package io.adavliatov.transfer.model.transaction;

import io.adavliatov.alpha.model.Id;
import io.adavliatov.alpha.model.Model;
import io.adavliatov.transfer.model.account.Account;
import lombok.ToString;
import org.javamoney.moneta.Money;

import java.util.Optional;
import java.util.UUID;

import static io.adavliatov.alpha.model.Id.Companion;
import static java.util.UUID.randomUUID;

/**
 * Encapsulates money transfer from one account to another.
 *
 * @author adavliatov
 * @since 22.03.2017
 */
@ToString
public final class Transaction extends Model<Transaction, Transaction.State> {

    private final UUID id = randomUUID();
    private final Optional<Account> from;
    private final Account to;
    private final Money amount;
    private final TransactionType operation;

    public Transaction(final Id<Transaction> id,
                       final State state,
                       final TransactionType type, final Optional<Account> from,
                       final Account to,
                       final Money amount) {
        super(id, state);
        this.from = from;
        this.to = to;
        this.amount = amount;
        this.operation = type;
    }

    public Transaction(final State state,
                       final TransactionType type, final Account from,
                       final Account to,
                       final Money amount) {
        this(Companion.randomId(Transaction.class), state, type, Optional.of(from), to, amount);
    }

    public Transaction(final State state,
                       final TransactionType type, final Account to,
                       final Money amount) {
        this(Companion.randomId(Transaction.class), state, type, Optional.empty(), to, amount);
    }

    public Optional<Account> from() {
        return from;
    }

    public Account to() {
        return to;
    }

    public Money amount() {
        return amount;
    }

    public TransactionType operation() {
        return operation;
    }

    public enum State implements io.adavliatov.alpha.model.State<Transaction> {
        success,
        failedInsuffecientFunds,
        failedInvalidAmount,
        failedInvalidToAccount,
        failedSameAccount,
    }

}
