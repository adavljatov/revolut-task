package io.adavliatov.transfer.model.error;

import io.adavliatov.alpha.model.error.IllegalStateError;

import static io.adavliatov.transfer.model.error.ErrorCode.DUPLICATE_ACCOUNT;

public class DuplicateAccountError extends IllegalStateError {

    public DuplicateAccountError(String name) {
        super(DUPLICATE_ACCOUNT.code, DUPLICATE_ACCOUNT.message, name);
    }

}
