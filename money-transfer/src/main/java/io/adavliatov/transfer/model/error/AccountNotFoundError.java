package io.adavliatov.transfer.model.error;

import io.adavliatov.alpha.model.error.NotFoundError;

import static io.adavliatov.transfer.model.error.ErrorCode.ACCOUNT_NOT_FOUND;

public class AccountNotFoundError extends NotFoundError {

    public AccountNotFoundError(String name) {
        super(ACCOUNT_NOT_FOUND.code, ACCOUNT_NOT_FOUND.message, name);
    }
}
