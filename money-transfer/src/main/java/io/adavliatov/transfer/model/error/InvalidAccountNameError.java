package io.adavliatov.transfer.model.error;

import io.adavliatov.alpha.model.error.IllegalArgumentError;

import static io.adavliatov.transfer.model.error.ErrorCode.INVALID_ACCOUNT_NAME;

public class InvalidAccountNameError extends IllegalArgumentError {

    public InvalidAccountNameError(String name) {
        super(INVALID_ACCOUNT_NAME.code, INVALID_ACCOUNT_NAME.message, name);
    }
}
