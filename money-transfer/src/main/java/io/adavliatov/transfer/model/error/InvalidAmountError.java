package io.adavliatov.transfer.model.error;

import io.adavliatov.alpha.model.error.IllegalArgumentError;
import org.javamoney.moneta.Money;

import static io.adavliatov.transfer.model.error.ErrorCode.INVALID_AMOUNT;

public class InvalidAmountError extends IllegalArgumentError {

    public InvalidAmountError(Money money) {
        super(INVALID_AMOUNT.code, INVALID_AMOUNT.message, money);
    }
}
