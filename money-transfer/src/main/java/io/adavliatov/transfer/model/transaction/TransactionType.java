package io.adavliatov.transfer.model.transaction;

/**
 * Represents transfer operation.
 */
public enum TransactionType {
    deposit,
    withdraw,
    transfer
}
