package io.adavliatov.transfer.model.account;

import io.adavliatov.alpha.model.Id;
import io.adavliatov.alpha.model.ModelWithoutState;
import io.adavliatov.transfer.model.transaction.Transaction;
import lombok.ToString;
import org.javamoney.moneta.Money;

import static io.adavliatov.alpha.model.Id.Companion;
import static io.adavliatov.transfer.model.transaction.Transaction.State.*;
import static io.adavliatov.transfer.model.transaction.TransactionType.*;

/**
 * Represents thread-safe user account business model.
 * Model is responsible for the bunch of operations.
 * //todo?: move to ActiveRecord for simplicity?
 */
//extension points: currently consider 1-1 user-account, then save separate User instance.
//todo?: introduce `Lock` object per instance?
@ToString
public class Account extends ModelWithoutState<Account> {

    private final String name;
    private Money balance;

    public Account(Id<Account> accountId, String name, Money balance) {
        super(accountId);
        this.name = name;
        this.balance = balance;
    }

    public Account(String name, Money balance) {
        this(Companion.randomId(Account.class), name, balance);
    }

    public String name() {
        return name;
    }

    /**
     * Deposits specified balance.
     *
     * @param amount the balance to deposit
     * @return the transaction representing deposit
     */
    public Transaction deposit(Money amount) {
        if (amount == null || amount.isNegativeOrZero()) {
            return new Transaction(failedInvalidAmount, deposit, this, amount);
        }
        synchronized (this) {
            balance = balance().add(amount);
            return new Transaction(success, deposit, this, amount);
        }
    }

    /**
     * Returns user balance.
     *
     * @return the current account balance
     */
    public synchronized Money balance() {
        return balance;
    }

    /**
     * Withdraw specified balance.
     *
     * @param amount the balance to withdraw
     * @return the transaction representing withdraw
     */
    //different strategies should be considered:
    //1. fail-fast (implemented);
    //2. wait for deposit;
    public Transaction withdraw(Money amount) {
        if (amount == null || amount.isNegativeOrZero()) {
            return new Transaction(failedInvalidAmount, deposit, this, amount);
        }
        synchronized (this) {
            if (this.balance().isGreaterThan(amount)) {
                balance = balance().subtract(amount);
                return new Transaction(success, withdraw, this, amount);
            } else {
                return new Transaction(failedInsuffecientFunds, withdraw, this, amount);
            }
        }
    }

    /**
     * Transfers the specified balance from this account to the `to` account.
     *
     * @param to     the target account
     * @param amount the balance to transfer
     * @return the transaction representing transfer
     */
    public Transaction transfer(Account to, Money amount) {
        if (to == null) {
            return new Transaction(failedInvalidToAccount, transfer, this, amount);
        }
        if (amount == null || amount.isNegativeOrZero()) {
            return new Transaction(failedInvalidAmount, transfer, this, amount);
        }
        if (this.equals(to)) {
            return new Transaction(failedSameAccount, transfer, this, amount);
        }
        final Account lock1;
        final Account lock2;
        if (this.name().compareTo(to.name()) < 0) {
            lock1 = this;
            lock2 = to;
        } else {
            lock1 = to;
            lock2 = this;
        }
        synchronized (lock1) {
            synchronized (lock2) {
                final Transaction withdrawTx = this.withdraw(amount);
                if (success == withdrawTx.getState()) {
                    final Transaction depositTx = to.deposit(amount);
                    return new Transaction(success, transfer, this, to, amount);
                } else {
                    return new Transaction(withdrawTx.getState(), transfer, this, to, amount);
                }
            }
        }
    }
}
