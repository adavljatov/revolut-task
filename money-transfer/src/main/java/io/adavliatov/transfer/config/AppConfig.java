package io.adavliatov.transfer.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@ToString
@EqualsAndHashCode
public final class AppConfig {

    @JsonProperty("server.port")
    public int port;
    @JsonProperty("partitions.size")
    public int partitions;

}
