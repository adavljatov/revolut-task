package io.adavliatov.transfer.repo.account;

import io.adavliatov.transfer.model.account.Account;

import java.util.Optional;

/**
 * Account repository.
 *
 * @author adavliatov
 * @since 22.03.2017
 */
public interface AccountRepo {

    /**
     * Returns the account if exists or create a new one otherwise
     *
     * @param name account name (used as unique id)
     * @return Optional.empty() in case of the invalid name otherwise Optional.of(account)
     */
    Optional<Account> getOrCreate(String name);

    /**
     * Searches for the account
     *
     * @param name account name to search for
     * @return Optional.empty() in case of the invalid name or non-existing account
     * otherwise Optional.of(account)
     */
    Optional<Account> findByName(String name);
}
