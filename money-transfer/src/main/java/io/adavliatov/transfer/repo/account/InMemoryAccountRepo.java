package io.adavliatov.transfer.repo.account;

import io.adavliatov.transfer.model.account.Account;
import org.javamoney.moneta.Money;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Optional.*;
import static org.apache.commons.lang3.StringUtils.isAlphanumeric;

/**
 * {@inheritDoc}
 */
public class InMemoryAccountRepo implements AccountRepo {

    private final Map<String, Account> accounts = new ConcurrentHashMap<>();

    @Override
    public Optional<Account> getOrCreate(String name) {
        if (!isAlphanumeric(name) || Character.isDigit(name.charAt(0))) return empty();
        //extension points: duplication: throw error? ignore?
        return of(accounts.computeIfAbsent(name, s -> new Account(name, Money.of(0, "GBP"))));
    }

    @Override
    public Optional<Account> findByName(String name) {
        if (!isAlphanumeric(name) || Character.isDigit(name.charAt(0))) return empty();
        return ofNullable(accounts.get(name));
    }
}
