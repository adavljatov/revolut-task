package io.adavliatov.transfer.service.account;

import com.google.inject.Inject;
import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.error.InvalidAccountNameError;
import io.adavliatov.transfer.repo.account.AccountRepo;

import java.util.Map;
import java.util.Optional;

import static java.lang.Math.floorMod;
import static org.apache.commons.lang3.StringUtils.isAlphanumeric;

public class InMemoryAccountService implements AccountService {

    /**
     * Sharded Account repositories
     */
    private Map<Integer, AccountRepo> accountRepos;

    @Inject
    public InMemoryAccountService(Map<Integer, AccountRepo> accountRepos) {
        this.accountRepos = accountRepos;
    }

    @Override
    public Account getOrCreate(String name) throws InvalidAccountNameError {
        if (!isAlphanumeric(name) || Character.isDigit(name.charAt(0))) {
            throw new InvalidAccountNameError(name);
        }
        return getRepoByHash(name).getOrCreate(name).orElseThrow(() -> new InvalidAccountNameError(name));
    }

    @Override
    public Optional<Account> findByName(String name) {
        if (!isAlphanumeric(name) || Character.isDigit(name.charAt(0))) {
            return Optional.empty();
        }
        return getRepoByHash(name).findByName(name);
    }

    private AccountRepo getRepoByHash(String name) {
        return accountRepos.get(floorMod(name.hashCode(), accountRepos.size()));
    }
}
