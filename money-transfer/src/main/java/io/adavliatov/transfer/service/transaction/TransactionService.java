package io.adavliatov.transfer.service.transaction;

import io.adavliatov.transfer.model.transaction.Transaction;
import org.javamoney.moneta.Money;

/**
 * Service responsible for transactions handling.
 */
public interface TransactionService {

    /**
     * Increases balance for the specified account (if exists & valid balance as parameter)
     *
     * @param toAccountName the account name to deposit to
     * @param amount        the specified balance
     * @return the {@link Transaction} representing the deposit
     */
    Transaction deposit(String toAccountName, Money amount);

    /**
     * Decreases balance for the specified account (if exists & valid balance as parameter)
     *
     * @param fromAccountName the account name to withdraw from
     * @param amount          the specified balance
     * @return the {@link Transaction} representing the deposit
     */
    Transaction withdraw(String fromAccountName, Money amount);

    /**
     * Transfers balance from account to another.
     *
     * @param toAccountName the account name to deposit to
     * @param amount        the specified balance
     * @return the {@link Transaction} representing the deposit
     */
    Transaction transfer(String fromAccountName, String toAccountName, Money amount);
}
