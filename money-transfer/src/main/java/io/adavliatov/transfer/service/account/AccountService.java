package io.adavliatov.transfer.service.account;

import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.error.AccountNotFoundError;
import io.adavliatov.transfer.model.error.InvalidAccountNameError;

import java.util.Optional;

/**
 * Account management service.
 */
public interface AccountService {

    /**
     * Returns the existing account or create a new one.
     *
     * @param name account unique name
     * @return created account
     * @throws InvalidAccountNameError in case not alphanumeric or starts with digit
     */
    Account getOrCreate(String name);

    /**
     * Searches for the account with specified name
     *
     * @param name account name to be find
     * @return Optional: account if exists or empty otherwise
     * @throws InvalidAccountNameError in case not alphanumeric or starts with digit
     */
    Optional<Account> findByName(String name);

    default Account getByName(String name) {
        return findByName(name).orElseThrow(() -> new AccountNotFoundError(name));
    }
}
