package io.adavliatov.transfer.service.transaction;

import com.google.inject.Inject;
import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.transaction.Transaction;
import io.adavliatov.transfer.service.account.AccountService;
import org.javamoney.moneta.Money;

public class InMemoryTransactionService implements TransactionService {

    private AccountService accountService;

    @Inject
    public InMemoryTransactionService(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public Transaction deposit(String toAccountName, Money amount) {
        return accountService.getByName(toAccountName).deposit(amount);
    }

    @Override
    public Transaction withdraw(String fromAccountName, Money amount) {
        return accountService.getByName(fromAccountName).withdraw(amount);
    }

    @Override
    public Transaction transfer(String fromAccountName, String toAccountName, Money amount) {
        final Account fromAccount = accountService.getByName(fromAccountName);
        final Account toAccount = accountService.getByName(toAccountName);
        return fromAccount.transfer(toAccount, amount);
    }
}
