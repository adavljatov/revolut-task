package io.adavliatov.transfer;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.common.collect.ImmutableSet;
import com.google.inject.Injector;
import com.google.inject.Module;
import io.adavliatov.transfer.api.AccountResource;
import io.adavliatov.transfer.api.ExceptionsResource;
import io.adavliatov.transfer.api.TransactionResource;
import io.adavliatov.transfer.config.AppConfig;
import io.adavliatov.transfer.module.BootstrapModule;

import java.io.IOException;

import static com.fasterxml.jackson.core.JsonGenerator.Feature.IGNORE_UNKNOWN;
import static com.google.inject.Guice.createInjector;
import static java.util.Arrays.asList;
import static spark.Spark.awaitInitialization;
import static spark.Spark.port;

/**
 * Application entry point.
 */
public class MoneyTransferApp {

    protected final AppConfig config;
    public final Injector injector;

    public MoneyTransferApp(final AppConfig config, final Module... overrides) {
        this.config = config;
        this.injector = createInjector(ImmutableSet.<Module>builder()
            .add(new BootstrapModule(config))
            .addAll(asList(overrides))
            .build());
    }

    public static void main(String[] args) throws IOException {
        final AppConfig config = createAppConfig("app.config.yml");
        final MoneyTransferApp app = new MoneyTransferApp(config);
        app.start();
    }

    public void start() {
        port(config.port);
        registerEndpoints(injector);
        awaitInitialization();
    }

    private static void registerEndpoints(Injector injector) {
        injector.getInstance(TransactionResource.class).registerEndpoints();
        injector.getInstance(AccountResource.class).registerEndpoints();
        injector.getInstance(ExceptionsResource.class).registerEndpoints();
    }

    public static AppConfig createAppConfig(final String configPath) throws IOException {
        return new ObjectMapper(new YAMLFactory()
            .enable(IGNORE_UNKNOWN))
            .readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(configPath), AppConfig.class);
    }

}
