package io.adavliatov.transfer.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.MapBinder;
import io.adavliatov.transfer.config.AppConfig;
import io.adavliatov.transfer.repo.account.AccountRepo;
import io.adavliatov.transfer.repo.account.InMemoryAccountRepo;
import io.adavliatov.transfer.service.account.AccountService;
import io.adavliatov.transfer.service.account.InMemoryAccountService;
import io.adavliatov.transfer.service.transaction.InMemoryTransactionService;
import io.adavliatov.transfer.service.transaction.TransactionService;

import java.util.stream.IntStream;

import static com.google.inject.multibindings.MapBinder.newMapBinder;

public class BootstrapModule extends AbstractModule {

    private final AppConfig appConfig;

    public BootstrapModule(final AppConfig appConfig) {
        this.appConfig = appConfig;
    }

    @Override
    protected void configure() {
        //services
        bind(AccountService.class).to(InMemoryAccountService.class).in(Singleton.class);
        bind(TransactionService.class).to(InMemoryTransactionService.class).in(Singleton.class);

        //dao (+ sharding)
        final MapBinder<Integer, AccountRepo> repoMapBinder = newMapBinder(binder(), Integer.class, AccountRepo.class);
        IntStream.range(0, appConfig.partitions).forEach(i -> repoMapBinder.addBinding(i).toInstance(new InMemoryAccountRepo()));
    }
}
