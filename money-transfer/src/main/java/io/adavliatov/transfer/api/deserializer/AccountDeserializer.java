package io.adavliatov.transfer.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import io.adavliatov.alpha.model.Id;
import io.adavliatov.transfer.model.account.Account;
import org.javamoney.moneta.Money;

import java.lang.reflect.Type;
import java.util.UUID;

public final class AccountDeserializer implements JsonDeserializer<Account> {

    @Override
    public Account deserialize(JsonElement jElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jObject = jElement.getAsJsonObject();
        final Id<Account> id = new Id<>(Account.class, UUID.fromString(jObject.get("id").getAsString()));
        final String name = jObject.get("name").getAsString();
        final Money balance = context.deserialize(jObject.getAsJsonObject("balance"), Money.class);
        return new Account(id, name, balance);
    }
}
