package io.adavliatov.transfer.api.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.adavliatov.transfer.model.transaction.Transaction;

import java.lang.reflect.Type;

public final class TransactionSerializer implements JsonSerializer<Transaction> {

    @Override
    public JsonElement serialize(final Transaction txn, final Type typeOfSrc, final JsonSerializationContext context) {
        final JsonObject json = new JsonObject();

        json.addProperty("id", txn.getId().getId().toString());
        json.addProperty("state", txn.getState().name());
        txn.from().ifPresent(fromAccount -> json.add("from", context.serialize(fromAccount)));
        json.add("to", context.serialize(txn.to()));
        json.add("amount", context.serialize(txn.amount()));
        json.addProperty("type", txn.operation().name());

        return json;
    }
}
