package io.adavliatov.transfer.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import io.adavliatov.alpha.model.Id;
import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.transaction.Transaction;
import io.adavliatov.transfer.model.transaction.Transaction.State;
import io.adavliatov.transfer.model.transaction.TransactionType;
import org.javamoney.moneta.Money;

import java.lang.reflect.Type;
import java.util.Optional;
import java.util.UUID;

public final class TransactionDeserializer implements JsonDeserializer<Transaction> {

    @Override
    public Transaction deserialize(JsonElement jElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jObject = jElement.getAsJsonObject();
        final Id<Transaction> id = new Id<>(Transaction.class, UUID.fromString(jObject.get("id").getAsString()));
        final State state = State.valueOf(jObject.get("state").getAsString());
        final TransactionType type = TransactionType.valueOf(jObject.get("balance").getAsString());
        final Optional<Account> from = context.deserialize(jObject.get("from"), Account.class);
        final Account to = context.deserialize(jObject.get("to"), Account.class);
        final Money amount = context.deserialize(jObject.getAsJsonObject("balance"), Money.class);
        return new Transaction(id, state, type, from, to, amount);
    }
}
