package io.adavliatov.transfer.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import org.javamoney.moneta.Money;

import java.lang.reflect.Type;
import java.math.BigDecimal;

public final class MoneyDeserializer implements JsonDeserializer<Money> {

    @Override
    public Money deserialize(JsonElement jElement, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        final JsonObject jObject = jElement.getAsJsonObject();
        final BigDecimal amount = jObject.get("amount").getAsBigDecimal();
        final String currency = jObject.get("currency").getAsString();
        return Money.of(amount, currency);
    }
}
