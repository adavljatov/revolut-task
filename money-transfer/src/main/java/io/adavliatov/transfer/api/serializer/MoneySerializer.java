package io.adavliatov.transfer.api.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.javamoney.moneta.Money;

import java.lang.reflect.Type;

public final class MoneySerializer implements JsonSerializer<Money> {

    @Override
    public JsonElement serialize(final Money money, final Type typeOfSrc, final JsonSerializationContext context) {
        final JsonObject jsonMoney = new JsonObject();

        jsonMoney.addProperty("amount", money.getNumber());
        jsonMoney.addProperty("currency", money.getCurrency().getCurrencyCode());

        return jsonMoney;
    }
}
