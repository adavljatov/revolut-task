package io.adavliatov.transfer.api.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import io.adavliatov.transfer.model.account.Account;

import java.lang.reflect.Type;

public final class AccountSerializer implements JsonSerializer<Account> {

    @Override
    public JsonElement serialize(final Account account, final Type typeOfSrc, final JsonSerializationContext context) {
        final JsonObject json = new JsonObject();

        json.addProperty("id", account.getId().getId().toString());
        json.addProperty("name", account.name());
        json.add("balance", context.serialize(account.balance()));

        return json;
    }
}
