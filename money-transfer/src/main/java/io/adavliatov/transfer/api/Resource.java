package io.adavliatov.transfer.api;

import com.google.gson.GsonBuilder;
import io.adavliatov.transfer.api.deserializer.AccountDeserializer;
import io.adavliatov.transfer.api.deserializer.MoneyDeserializer;
import io.adavliatov.transfer.api.deserializer.TransactionDeserializer;
import io.adavliatov.transfer.api.serializer.AccountSerializer;
import io.adavliatov.transfer.api.serializer.MoneySerializer;
import io.adavliatov.transfer.api.serializer.TransactionSerializer;
import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.transaction.Transaction;
import org.javamoney.moneta.Money;
import spark.Request;
import spark.Response;
import spark.ResponseTransformer;
import spark.Route;

import java.util.Optional;
import java.util.function.BiFunction;

import static java.util.Optional.ofNullable;

public abstract class Resource {

    public static String CONTENT_TYPE_JSON = "application/json";
    static GsonBuilder gsonBuilder = new GsonBuilder()
        .registerTypeAdapter(Money.class, new MoneyDeserializer())
        .registerTypeAdapter(Money.class, new MoneySerializer())
        .registerTypeAdapter(Account.class, new AccountSerializer())
        .registerTypeAdapter(Account.class, new AccountDeserializer())
        .registerTypeAdapter(Transaction.class, new TransactionSerializer())
        .registerTypeAdapter(Transaction.class, new TransactionDeserializer());
    static ResponseTransformer jsonTransformer = model -> gsonBuilder.create().toJson(model);

    public abstract void registerEndpoints();

    protected static <Rq> Rq requestAs(Request rq, Class<Rq> requestClass) {
        return gsonBuilder.create().fromJson(rq.body(), requestClass);
    }

    protected static <Rq> Optional<Rq> maybeRequestAs(Request rq, Class<Rq> requestClass) {
        return ofNullable(rq.body()).map(req -> gsonBuilder.create().fromJson(req, requestClass));
    }

    protected static Route ok(BiFunction<Request, Response, Object> mapper) {
        return (req, res) -> {
            res.type(CONTENT_TYPE_JSON);
            return gsonBuilder.create().toJson(mapper.apply(req, res));
        };
    }

    protected static <Rq> Route ok(BiFunction<Rq, Response, Object> mapper, Class<Rq> requestClass) {
        return (rq, rs) -> {
            rs.type(CONTENT_TYPE_JSON);
            return gsonBuilder.create().toJson(mapper.apply(requestAs(rq, requestClass), rs));
        };
    }
}
