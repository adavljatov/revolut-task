package io.adavliatov.transfer.api;

import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.service.account.AccountService;
import lombok.extern.slf4j.Slf4j;
import spark.Request;
import spark.Response;

import javax.inject.Inject;

import static spark.Spark.get;
import static spark.Spark.post;

@Slf4j
public class AccountResource extends Resource {

    private final AccountService accountService;

    @Inject
    public AccountResource(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void registerEndpoints() {
        post("/account", ok(this::getOrCreate));
        get("/account/:name", ok(this::one));
    }

    private Account getOrCreate(Request rq, Response rs) {
        final CreateAccountRequest request = requestAs(rq, CreateAccountRequest.class);
        return accountService.getOrCreate(request.name);
    }

    private Account one(Request rq, Response rs) {
        final String accountName = rq.params("name");
        return accountService.getByName(accountName);
    }

    private static class CreateAccountRequest {

        public String name;
    }
}
