package io.adavliatov.transfer.api;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import io.adavliatov.alpha.model.error.AppError;

import static org.eclipse.jetty.http.HttpStatus.NOT_FOUND_404;
import static spark.Spark.exception;
import static spark.Spark.notFound;

public class ExceptionsResource extends Resource {

    @Override
    public void registerEndpoints() {
        notFound((req, res) -> {
            res.type(CONTENT_TYPE_JSON);
            res.status(NOT_FOUND_404);
            return "{\"error\":\"Not found\"}";
        });

        handleException(AppError.class);
    }

    private void handleException(Class<? extends AppError> appErrorClass) {
        exception(appErrorClass, (ex, rq, rs) -> {
            final AppError error = (AppError) ex;
            rs.type(CONTENT_TYPE_JSON);
            rs.status(error.getHttpErrorCode().getHttpCode());

            final JsonObject jsonError = new JsonObject();
            jsonError.addProperty("errorCode", error.getErrorCode());
            jsonError.addProperty("errorMessage", error.getMessage());

            rs.body(new Gson().toJson(jsonError));
        });
    }

}
