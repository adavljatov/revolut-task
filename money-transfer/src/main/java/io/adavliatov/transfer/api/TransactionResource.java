package io.adavliatov.transfer.api;

import com.google.inject.Inject;
import io.adavliatov.transfer.model.error.AccountNotFoundError;
import io.adavliatov.transfer.model.error.InvalidAccountNameError;
import io.adavliatov.transfer.model.transaction.Transaction;
import io.adavliatov.transfer.service.transaction.TransactionService;
import lombok.extern.slf4j.Slf4j;
import org.javamoney.moneta.Money;
import spark.Request;
import spark.Response;

import static spark.Spark.path;
import static spark.Spark.post;

@Slf4j
public class TransactionResource extends Resource {

    private final TransactionService transactionService;

    @Inject
    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @Override
    public void registerEndpoints() {
        path("/tx", () -> {
            post("/deposit", this::deposit, jsonTransformer);
            post("/withdraw", this::withdraw, jsonTransformer);
            post("/transfer", this::transfer, jsonTransformer);
        });
    }

    private Transaction deposit(Request rq, Response rs) throws InvalidAccountNameError, AccountNotFoundError {
        final DepositRequest deposit = requestAs(rq, DepositRequest.class);
        return transactionService.deposit(deposit.name, deposit.amount);
    }

    private Transaction withdraw(Request rq, Response rs) throws InvalidAccountNameError, AccountNotFoundError {
        final WithdrawRequest withdraw = requestAs(rq, WithdrawRequest.class);
        return transactionService.withdraw(withdraw.name, withdraw.amount);
    }

    private Transaction transfer(Request rq, Response rs) throws InvalidAccountNameError, AccountNotFoundError {
        final TransferRequest transfer = requestAs(rq, TransferRequest.class);
        return transactionService.transfer(transfer.from, transfer.to, transfer.amount);
    }

    private static final class DepositRequest {

        public String name;
        public Money amount;
    }

    private static final class WithdrawRequest {

        public String name;
        public Money amount;
    }

    private static final class TransferRequest {

        public String from;
        public String to;
        public Money amount;
    }

}
