package io.adavliatov.transfer.api

import com.google.inject.Injector
import io.adavliatov.transfer.MoneyTransferApp
import io.adavliatov.transfer.config.AppConfig
import org.apache.http.HttpHost
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.StringEntity
import org.apache.http.message.BasicNameValuePair
import org.apache.http.util.EntityUtils
import spock.lang.Specification

import static io.adavliatov.transfer.api.Resource.CONTENT_TYPE_JSON
import static java.nio.charset.StandardCharsets.UTF_8
import static java.util.Collections.emptyMap
import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic
import static org.apache.http.impl.client.HttpClients.createDefault

abstract class MoneyTransferSpec extends Specification {

    protected static HttpHost host
    protected static AppConfig config
    protected static HttpClient api
    protected static Injector injector

    static {
        config = MoneyTransferApp.createAppConfig('app-test.config.yml')
        def app = new MoneyTransferApp(config)
        app.start()
        injector = app.injector

        host = new HttpHost('localhost', config.port)
        api = createDefault()
    }

    def setup() {
        injector.injectMembers(this)
    }

    protected static def response(HttpResponse response) {
        return EntityUtils.toString(response.entity, UTF_8)
    }

    protected static def post(String uri) {
        def post = new HttpPost(uri)
        post.setHeader("Content-Type", CONTENT_TYPE_JSON)
        return post
    }

    protected static def post(String uri, String content) {
        def post = new HttpPost(uri)
        post.setHeader("Content-Type", CONTENT_TYPE_JSON)

        def entity = new StringEntity(content)
        entity.setContentType(CONTENT_TYPE_JSON)

        post.setEntity(entity)
        return post
    }

    protected static def get(String uri, Map<String, String> queryParams = emptyMap()) {
        def uriBuilder = new URIBuilder(uri)
        uriBuilder.addParameters(queryParams.collect { k, v -> new BasicNameValuePair(k, v) })

        def get = new HttpGet(uriBuilder.build())
        get.setHeader("Content-Type", CONTENT_TYPE_JSON)
        return get
    }

    protected static String aRandomName() {
        return randomAlphabetic(10);
    }

}
