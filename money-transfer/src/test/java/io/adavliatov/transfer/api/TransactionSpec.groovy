package io.adavliatov.transfer.api

import io.adavliatov.transfer.model.account.Account
import io.adavliatov.transfer.service.account.AccountService
import io.adavliatov.transfer.service.transaction.TransactionService
import org.javamoney.moneta.Money
import spock.lang.Shared

import javax.inject.Inject

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson
import static org.junit.jupiter.api.Assertions.assertEquals

class TransactionSpec extends MoneyTransferSpec {

    @Inject
    AccountService accountService
    @Inject
    TransactionService transactionService

    @Shared
    Account from
    @Shared
    Account to

    def setup() {
        from = accountService.getOrCreate(aRandomName())
        to = accountService.getOrCreate(aRandomName())
        transactionService.deposit(from.name(), Money.of(10, "GBP"))
        transactionService.deposit(to.name(), Money.of(10, "GBP"))
    }

    def 'POST /tx/deposit should deposit account'() {
        when:
        def json = response(api.execute(host, post("/tx/deposit", """
            {
              "name": "${to.name()}",
              "amount": {
                "amount": 5,
                "currency": "GBP"
              }
            }
        """ as String)))

        then:
        assertThatJson(json)
            .isEqualTo("""
                {
                  "id": "\${json-unit.ignore}",
                  "state": "success",
                  "to": {
                    "id": "${to.id.getId()}",
                    "name": "${to.name()}",
                    "balance": {
                      "amount": 15,
                      "currency": "GBP"
                    }
                  },
                  "amount": {
                    "amount": 5,
                    "currency": "GBP"
                  },
                  "type": "deposit"
                }""" as String)
        assertEquals(accountService.getByName(to.name()).balance(), Money.of(15, "GBP"))
    }

    def 'POST /tx/deposit should fail if account not found'() {
        when:
        def name = aRandomName()
        def rs = api.execute(host, post("/tx/deposit", """
            {
              "name": "${name}",
              "amount": {
                "amount": 5,
                "currency": "GBP"
              }
            }""" as String))


        then:
        rs.statusLine.statusCode == 404
        assertThatJson(response(rs))
            .isEqualTo("""
              {
                "errorCode":20, 
                "errorMessage":"Account(s) not found: $name."
              }""" as String)
    }

    def 'POST /tx/withdraw should withdraw account'() {
        when:
        def json = response(api.execute(host, post("/tx/withdraw", """
            {
              "name": "${from.name()}",
              "amount": {
                "amount": 5,
                "currency": "GBP"
              }
            }
        """ as String)))

        then:
        assertThatJson(json)
            .isEqualTo("""
                {
                  "id": "\${json-unit.ignore}",
                  "state": "success",
                  "to": {
                    "id": "${from.id.getId()}",
                    "name": "${from.name()}",
                    "balance": {
                      "amount": 5,
                      "currency": "GBP"
                    }
                  },
                  "amount": {
                    "amount": 5,
                    "currency": "GBP"
                  },
                  "type": "withdraw"
                }""" as String)
        assertEquals(accountService.getByName(from.name()).balance(), Money.of(5, "GBP"))
    }

    def 'POST /tx/withdraw should fail if account not found'() {
        when:
        def name = aRandomName()
        def rs = api.execute(host, post("/tx/withdraw", """
            {
              "name": "${name}",
              "amount": {
                "amount": 5,
                "currency": "GBP"
              }
            }""" as String))


        then:
        rs.statusLine.statusCode == 404
        assertThatJson(response(rs))
            .isEqualTo("""
              {
                "errorCode":20, 
                "errorMessage":"Account(s) not found: $name."
              }""" as String)
    }

    def 'POST /tx/transfer should transfer money from one account to another'() {
        when:
        def json = response(api.execute(host, post("/tx/transfer", """
            {
              "from": "${from.name()}",
              "to": "${to.name()}",
              "amount": {
                "amount": 5,
                "currency": "GBP"
              }
            }
        """ as String)))

        then:
        assertThatJson(json)
            .isEqualTo("""
              {
                  "id": "\${json-unit.ignore}",
                  "state": "success",
                  "from": {
                    "id": "${from.id.getId()}",
                    "name": "${from.name()}",
                    "balance": {
                      "amount": 5,
                      "currency": "GBP"
                    }
                  },
                  "to": {
                    "id": "${to.id.getId()}",
                    "name": "${to.name()}",
                    "balance": {
                      "amount": 15,
                      "currency": "GBP"
                    }
                  },
                  "amount": {
                    "amount": 5,
                    "currency": "GBP"
                  },
                  "type": "transfer"
              }""" as String)
        assertEquals(accountService.getByName(from.name()).balance(), Money.of(5, "GBP"))
        assertEquals(accountService.getByName(to.name()).balance(), Money.of(15, "GBP"))
    }

    def 'POST /tx/transfer should fail if source account not found'() {
        when:
        def name = aRandomName()
        def rs = api.execute(host, post("/tx/transfer", """
            {
              "from": "${name}",
              "to": "${to.name()}",
              "amount": {
                "amount": 100,
                "currency": "GBP"
              }
            }""" as String))


        then:
        rs.statusLine.statusCode == 404
        assertThatJson(response(rs))
            .isEqualTo("""
              {
                "errorCode":20, 
                "errorMessage":"Account(s) not found: $name."
              }""" as String)
    }

    def 'POST /tx/transfer should fail if target account not found'() {
        when:
        def name = aRandomName()
        def rs = api.execute(host, post("/tx/transfer", """
            {
              "from": "${from.name()}",
              "to": "${name}",
              "amount": {
                "amount": 100,
                "currency": "GBP"
              }
            }""" as String))


        then:
        rs.statusLine.statusCode == 404
        assertThatJson(response(rs))
            .isEqualTo("""
              {
                "errorCode":20, 
                "errorMessage":"Account(s) not found: $name."
              }""" as String)
    }

}
