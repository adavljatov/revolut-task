package io.adavliatov.transfer.api

import io.adavliatov.transfer.service.account.AccountService
import spock.lang.Shared

import javax.inject.Inject

import static net.javacrumbs.jsonunit.fluent.JsonFluentAssert.assertThatJson

class AccountSpec extends MoneyTransferSpec {

    @Inject
    AccountService accountService

    @Shared
    String name

    def setup() {
        name = aRandomName()
    }

    def 'POST /account should create account'() {
        when:
        def json = response(api.execute(host, post("/account", """
            {
              "name": "$name"
            }
        """ as String)))

        then:
        assertThatJson(json)
            .isEqualTo("""
              {
                  "id": "\${json-unit.ignore}",
                  "name": "${name}",
                  "balance": {
                    "amount": 0,
                    "currency": "GBP"
                  }
              }""" as String)
    }

    def 'GET /account/:name should retrieve account'() {
        given:
        def account = accountService.getOrCreate(name)

        when:
        def json = response(api.execute(host, get("/account/${account.name()}")))

        then:
        assertThatJson(json)
            .isEqualTo("""
              {
                  "id": "${account.id.id}",
                  "name": "${name}",
                  "balance": {
                    "amount": 0,
                    "currency": "GBP"
                  }
              }""" as String)
    }

    def 'GET /account/:name should throw error if account not found'() {
        when:
        def rs = api.execute(host, get("/account/$name"))

        then:
        rs.statusLine.statusCode == 404
        assertThatJson(response(rs))
            .isEqualTo("""
              {
                "errorCode":20, 
                "errorMessage":"Account(s) not found: $name."
              }""" as String)
    }

}
