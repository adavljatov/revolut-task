package io.adavliatov.transfer.service.account;

import io.adavliatov.transfer.api.MockitoExtension;
import io.adavliatov.transfer.model.error.InvalidAccountNameError;
import io.adavliatov.transfer.repo.account.AccountRepo;
import io.adavliatov.transfer.repo.account.InMemoryAccountRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Optional.empty;

/**
 * Application service test.
 *
 * @author adavliatov
 * @since 29.03.2017
 */
@ExtendWith(MockitoExtension.class)
class InMemoryAccountServiceTest {

    private InMemoryAccountService accountService;

    @BeforeEach
    void init() {
        Map<Integer, AccountRepo> accountRepos = Collections.singletonMap(0, new InMemoryAccountRepo());
        accountService = new InMemoryAccountService(accountRepos);
    }

    @TestFactory
    @DisplayName("Invalid name: getOrCreate must throw error")
    Stream<DynamicTest> whenPassedInvalidName_ShouldThrowInvalidAccountNameExceptionOnCreate(@Mock AccountRepo accountRepo) {
        return whenInvalidNamePassed(
            name -> DynamicTest.dynamicTest(
                        "Test name: " + name,
                () -> Assertions.assertThrows(InvalidAccountNameError.class, () -> accountService.getOrCreate(name))));
    }

    @TestFactory
    @DisplayName("Invalid name: findByName must be empty")
    Stream<DynamicTest> whenPassedInvalidName_ShouldReturnEmptyOnFindByName(@Mock AccountRepo accountRepo) {
        return whenInvalidNamePassed(
            name -> DynamicTest.dynamicTest(
                        "Test name: " + name,
                () -> Assertions.assertSame(empty(), accountService.findByName(name))));
    }

    private Stream<DynamicTest> whenInvalidNamePassed(Function<? super String, ? extends DynamicTest> mapper) {
        return Stream
                .of(
                        null,
                        "",
                        "_123&",
                        "   asdn123",
                        "asd 321",
                        "123123",
                        "\u2202\u2202",
                        "qwe "
                )
                .map(mapper);
    }

}