package io.adavliatov.transfer.service.transaction;

import io.adavliatov.transfer.api.MockitoExtension;
import io.adavliatov.transfer.model.account.Account;
import io.adavliatov.transfer.model.error.AccountNotFoundError;
import io.adavliatov.transfer.model.error.InvalidAccountNameError;
import io.adavliatov.transfer.repo.account.AccountRepo;
import io.adavliatov.transfer.repo.account.InMemoryAccountRepo;
import io.adavliatov.transfer.service.account.AccountService;
import io.adavliatov.transfer.service.account.InMemoryAccountService;
import org.apache.commons.lang3.tuple.Pair;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static io.adavliatov.transfer.model.transaction.Transaction.State.*;
import static org.junit.jupiter.api.Assertions.assertSame;

/**
 * Transaction service test.
 */
@ExtendWith(MockitoExtension.class)
class InMemoryTransactionServiceTest {

    private static final String VALID_ACCOUNT_NAME1 = "validAccountName1";
    private static final String VALID_ACCOUNT_NAME2 = "validAccountName2";
    private static final String VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS = "validAccountName3";
    private static final Money VALID_AMOUNT = Money.of(5, "GBP");

    private TransactionService transactionService;

    @BeforeEach
    void init() {
        //at first the repo was mocked however in this implementation it's redundant
        AccountRepo accountRepo1 = new InMemoryAccountRepo();
        Map<Integer, AccountRepo> accountRepos = Collections.singletonMap(0, accountRepo1);
        AccountService accountService = new InMemoryAccountService(accountRepos);
        try {
            accountService.getOrCreate(VALID_ACCOUNT_NAME1);
            final Account account = accountService.getOrCreate(VALID_ACCOUNT_NAME2);
            account.deposit(Money.of(10, "GBP"));
        } catch (InvalidAccountNameError ignored) {
        }
        transactionService = new InMemoryTransactionService(accountService);
    }

    //Deposit
    @TestFactory
    @DisplayName("Invalid account name & valid balance: deposit must throw AccountNotFoundError error")
    Stream<DynamicTest> whenPassedInvalidName_DepositMustThrowAccountNotFoundException() {
        return whenInvalid(name -> DynamicTest.dynamicTest("Test name: " + name,
            () -> Assertions.assertThrows(AccountNotFoundError.class, () -> transactionService.deposit(name, VALID_AMOUNT))));
    }

    @TestFactory
    @DisplayName("Invalid balance & valid name but account not exists: deposit must throw AccountNotFoundError error")
    Stream<DynamicTest> whenPassedInvalidAmountValidNameAccountNotExist_DepositThrowAccountNotFoundException() {
        return whenInvalidAmount(amount -> DynamicTest.dynamicTest("Test name: " + amount,
            () -> Assertions.assertThrows(AccountNotFoundError.class, () -> transactionService.deposit(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, amount))));
    }

    @TestFactory
    @DisplayName("Invalid balance & valid name & account exists : deposit must return failed invalid balance status")
    Stream<DynamicTest> whenPassedInvalidAmountValidNameAccountExist_DepositMustReturnFailedInvalidAmountStatus() {
        return whenInvalidAmount(amount -> DynamicTest.dynamicTest("Test name: " + amount,
            () -> assertSame(transactionService.deposit(VALID_ACCOUNT_NAME1, amount).getState(), failedInvalidAmount)));
    }

    @Test
    @DisplayName("Valid balance & valid name & account exists : deposit must return success status")
    void whenPassedInvalidAmountValidNameAccountExist_DepositMustReturnSuccessStatus() {
        try {
            assertSame(transactionService.deposit(VALID_ACCOUNT_NAME1, VALID_AMOUNT).getState(), success);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {

        }
    }

    //Withdraw
    @TestFactory
    @DisplayName("Invalid account name & valid balance: withdraw must throw AccountNotFoundError error")
    Stream<DynamicTest> whenPassedInvalidName_WithdrawMustThrowAccountNotFoundException() {
        return whenInvalid(name -> DynamicTest.dynamicTest("Test name: " + name,
            () -> Assertions.assertThrows(AccountNotFoundError.class, () -> transactionService.withdraw(name, VALID_AMOUNT))));
    }

    @TestFactory
    @DisplayName("Invalid balance & valid name but account not exists: withdraw must throw AccountNotFoundError error")
    Stream<DynamicTest> whenPassedInvalidAmountValidNameAccountNotExist_WithdrawThrowAccountNotFoundException() {
        return whenInvalidAmount(amount -> DynamicTest.dynamicTest("Test name: " + amount,
            () -> Assertions.assertThrows(AccountNotFoundError.class, () -> transactionService.withdraw(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, amount))));
    }

    @TestFactory
    @DisplayName("Invalid balance & valid name & account exists : withdraw must return failed invalid balance status")
    Stream<DynamicTest> whenPassedInvalidAmountValidNameAccountExist_WithdrawMustReturnFailedInvalidAmountStatus() {
        return whenInvalidAmount(amount -> DynamicTest.dynamicTest("Test name: " + amount,
            () -> assertSame(transactionService.withdraw(VALID_ACCOUNT_NAME1, amount).getState(), failedInvalidAmount)));
    }

    @Test
    @DisplayName("Valid balance & valid name & account exists but not enough funds : withdraw must return failedInsufficientFunds status")
    void whenPassedInvalidAmountValidNameAccountExist_WithdrawMustReturnFailedInsufficientFundsStatus() {
        try {
            assertSame(transactionService.withdraw(VALID_ACCOUNT_NAME1, VALID_AMOUNT).getState(), failedInsuffecientFunds);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {

        }
    }

    @Test
    @DisplayName("Valid balance & valid name & account exists & sufficient funds : withdraw must return success status")
    void whenPassedInvalidAmountValidNameAccountExist_WithdrawMustReturnSuccessStatus() {
        try {
//           balance=10
            assertSame(transactionService.withdraw(VALID_ACCOUNT_NAME2, VALID_AMOUNT).getState(), success);
//           balance=5
            assertSame(transactionService.withdraw(VALID_ACCOUNT_NAME2, VALID_AMOUNT).getState(), success);
//           balance=0
            assertSame(transactionService.withdraw(VALID_ACCOUNT_NAME2, VALID_AMOUNT).getState(), failedInsuffecientFunds);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {

        }
    }

    @TestFactory
    @DisplayName("1 valid & 1 (invalid || not existed) accounts & valid balance: transfer must throw AccountNotFoundError error")
    Stream<DynamicTest> whenPassedInvalidAccountsAndValidAmount_TransferMustThrowAccountNotFoundException() {
        return Stream
            .of(
                //invalid name
                Pair.<String, String>of(null, VALID_ACCOUNT_NAME1),
                Pair.of("", VALID_ACCOUNT_NAME1),
                Pair.of("_123&", VALID_ACCOUNT_NAME1),
                Pair.of("   asdn123", VALID_ACCOUNT_NAME1),
                Pair.of("asd 321", VALID_ACCOUNT_NAME1),
                Pair.of("123123", VALID_ACCOUNT_NAME1),
                Pair.of("\u2202\u2202", VALID_ACCOUNT_NAME1),
                Pair.of("qwe ", VALID_ACCOUNT_NAME1),
                Pair.<String, String>of(VALID_ACCOUNT_NAME1, null),

                Pair.of(VALID_ACCOUNT_NAME1, ""),
                Pair.of(VALID_ACCOUNT_NAME1, "_123&"),
                Pair.of(VALID_ACCOUNT_NAME1, "   asdn123"),
                Pair.of(VALID_ACCOUNT_NAME1, "asd 321"),
                Pair.of(VALID_ACCOUNT_NAME1, "123123"),
                Pair.of(VALID_ACCOUNT_NAME1, "\u2202\u2202"),
                Pair.of(VALID_ACCOUNT_NAME1, "qwe "),

                //not exist
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),
                Pair.of(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS),

                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1),
                Pair.of(VALID_ACCOUNT_NAME_ACCOUNT_NOT_EXISTS, VALID_ACCOUNT_NAME1)
            )
            .map(param -> DynamicTest.dynamicTest("Test name: " + param,
                () -> Assertions.assertThrows(AccountNotFoundError.class,
                    () -> transactionService.transfer(param.getLeft(), param.getRight(), VALID_AMOUNT))));
    }

    @TestFactory
    @DisplayName("Invalid balance & valid accounts : transfer must return failed invalid balance status")
    Stream<DynamicTest> whenPassedInvalidAmountValidNameAccountExist_TransferMustReturnFailedInvalidAmountStatus() {
        return whenInvalidAmount(amount -> DynamicTest.dynamicTest("Test name: " + amount,
            () -> assertSame(transactionService.transfer(VALID_ACCOUNT_NAME1, VALID_ACCOUNT_NAME2, amount).getState(), failedInvalidAmount)));
    }

    @Test
    @DisplayName("Valid balance & valid accounts but insufficient : transfer must return failed insufficient funds")
    void whenPassedInvalidAmountValidNameAccountExist_TransferMustReturnFailedInsufficientFundsStatus() {
        try {
            assertSame(transactionService.transfer(VALID_ACCOUNT_NAME2, VALID_ACCOUNT_NAME1, Money.of(11, "GBP")).getState(), failedInsuffecientFunds);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {
        }
    }

    @Test
    @DisplayName("Valid balance & valid accounts & sufficient : transfer must return success status")
    void whenPassedInvalidAmountValidNameAccountExist_TransferMustReturnSuccessStatus() {
        try {
            //balance=10
            assertSame(transactionService.transfer(VALID_ACCOUNT_NAME2, VALID_ACCOUNT_NAME1, Money.of(4, "GBP")).getState(), success);
            //balance=6
            assertSame(transactionService.transfer(VALID_ACCOUNT_NAME2, VALID_ACCOUNT_NAME1, Money.of(4, "GBP")).getState(), success);
            //balance=2
            assertSame(transactionService.transfer(VALID_ACCOUNT_NAME2, VALID_ACCOUNT_NAME1, Money.of(4, "GBP")).getState(), failedInsuffecientFunds);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {
        }
    }

    @Test
    @DisplayName("Valid balance & valid account but the same : transfer must return failed same account")
    void whenPassedInvalidAmountValidNameAccountExist_TransferMustReturnFailedSameAccountStatus() {
        try {
            assertSame(transactionService.transfer(VALID_ACCOUNT_NAME2, VALID_ACCOUNT_NAME2, Money.of(4, "GBP")).getState(), failedSameAccount);
        } catch (AccountNotFoundError | InvalidAccountNameError ignored) {
        }
    }

    private Stream<DynamicTest> whenInvalid(Function<? super String, ? extends DynamicTest> mapper) {
        return Stream
            .of(
                null,
                "",
                "_123&",
                "   asdn123",
                "asd 321",
                "123123",
                "\u2202\u2202",
                "qwe "
            )
            .map(mapper);
    }

    private Stream<DynamicTest> whenInvalidAmount(Function<? super Money, ? extends DynamicTest> mapper) {
        return Stream
            .of(
                null,
                Money.of(Integer.MIN_VALUE, "GBP"),
                Money.of(-1, "GBP"),
                Money.of(0, "GBP")
            )
            .map(mapper);
    }
}